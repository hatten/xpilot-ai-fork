#Jim O'Connor - May 2012
#Run: python3 Walle.py

# Wall-E the wall checking bot
#
# This bot uses a wall feeler to check the distance to each wall.
# The value of wall_feeler1 is equal to the distance of the wall,
# as long as the wall is within the range specified by the first
# parameter of the wall_feeler function. In this case, the range
# is 500. The wall feeler will point straight ahead of the bot
# when the final paramter is 0, like it is in this case. Changing
# that parameter to 180 will make the bot look behind itself, and
# more than one wall feeler can be used in conjunction to see how 
# close each wall is in relation to othe walls.
#
# For this assignment, you must make the bot turn around before it
# would hit a wall. One way that you might want to do this is by
# using a conditional to check if the wall is close, and then to
# turn a certain number of degrees, say 180.
#
# Important functions are listed at http://www.xpilot-ed.org/Docs/Reference/
 
import libpyAI as ai

#all of your code should go in this function, in between AImain and return
#every line in between AImain and return will be called every frame, which 
#is many times per second.
def AImain():


	wall_feeler1 = ai.wallFeeler(500, int(ai.selfHeadingDeg()), 0, 0)
	
	print("The wall is",wall_feeler1,"pixels away.")
	

	return

ai.start(AImain,["-name","Walle"])
